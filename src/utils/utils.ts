import type { Video, Filters, Genre } from "./../types";

export const removeDuplicatesInArray = (array: Array<any>) => {
  return Array.from(new Set(array));
};

export const getYears = (videos: Video[]) => {
  return videos.map((video: Video) => video.release_year);
};

export const mapVideoToOtherGenre = (videos: Video[], genres: Genre[]) => {
  const genresIDs = genres.map((genre: Genre) => genre.id);
  return videos.map((video: Video) => {
    if (!genresIDs.includes(video.genre_id)) {
      return { ...video, genre_id: -1 };
    }
    return video;
  });
};

export const filterByInputQuery = (videos: Video[], query: string) => {
  const trimmedQuery = query.trim();
  if (trimmedQuery.length === 0) {
    return videos;
  } else {
    return videos.filter(
      (video: any) =>
        video.title
          .toString()
          .toLowerCase()
          .includes(trimmedQuery.toLowerCase()) ||
        video.artist
          .toString()
          .toLowerCase()
          .includes(trimmedQuery.toLowerCase())
    );
  }
};

export const filterByYear = (videos: Video[], year: number) => {
  if (year === 0) {
    return videos;
  }
  return videos.filter((video: any) => video.release_year === year);
};

export const filterByGenre = (videos: Video[], genresID: number[]) => {
  if (videos.length === 0 || genresID.length === 0) {
    return videos;
  } else {
    return videos
      .filter((video: any) => {
        if (!genresID.includes(video.genre_id)) {
          return false;
        }
        return true;
      })
      .map((video: any) => video);
  }
};

export const getFilteredVideos = (videos: Video[], filters: Filters) => {
  const inputQueryFiltered = filterByInputQuery(videos, filters.queryString);
  const yearsFiltered = filterByYear(inputQueryFiltered, filters.releaseYear);
  const genresFiltered = filterByGenre(yearsFiltered, filters.genresIDs);

  const filteredVideos = genresFiltered;

  return filteredVideos;
};
