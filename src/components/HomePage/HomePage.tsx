import { useState } from "react";

import { FiltersContainer, VideoList } from "./../../components";
import AppLayout from "../../layouts/App/AppLayout";

import {
  removeDuplicatesInArray,
  getYears,
  getFilteredVideos,
  mapVideoToOtherGenre,
} from "./../../utils/utils";

import type { Filters, TData } from "./../../types";

type HomePageProps = {
  data: TData;
};

const HomePage = ({ data }: HomePageProps) => {
  const [filters, setFilters] = useState<Filters>({
    queryString: "",
    releaseYear: 0,
    genresIDs: [],
  });

  const genres = [...data.genres, { id: -1, name: "Other" }];

  const onSearchHandler = (queryString: string) => {
    setFilters({
      ...filters,
      queryString,
    });
  };

  const onSelectGenreHandler = (genresIDs: number[]) => {
    setFilters({
      ...filters,
      genresIDs,
    });
  };

  const onSelectYearHandler = (releaseYear: number) => {
    setFilters({
      ...filters,
      releaseYear,
    });
  };

  const years = removeDuplicatesInArray(getYears(data.videos)).sort().reverse();

  return (
    <AppLayout>
      <FiltersContainer
        genres={genres}
        years={years}
        onSearch={onSearchHandler}
        onSelectGenre={onSelectGenreHandler}
        onSelectYear={onSelectYearHandler}
      />

      {data.videos.length > 0 ? (
        <VideoList
          videos={getFilteredVideos(
            mapVideoToOtherGenre(data.videos, genres),
            filters
          )}
          genres={genres}
        />
      ) : (
        <h4 style={{ color: "white" }}>No videos found</h4>
      )}
    </AppLayout>
  );
};

export default HomePage;
