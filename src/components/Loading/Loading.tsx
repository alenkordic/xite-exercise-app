import React from "react";
import { Box, CircularProgress, Typography } from "@mui/material";

import useStyles from "./Loading.styles";

const Loading = () => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <CircularProgress
        color="inherit"
        classes={{ root: classes.spinnerRoot }}
      />
      <Typography>Loading videos...</Typography>
    </Box>
  );
};

export default Loading;
