import { makeStyles, createStyles } from "@mui/styles";

export default makeStyles((theme) =>
  createStyles({
    root: {
      border: 0,
      color: "grey",
      width: "100%",
      height: "100vh",
      position: "fixed",
      display:"flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column"
      
    },
    spinnerRoot: {
      fontSize: "50px",
      width: "600px"
    },
  })
);
