import { Box, Grid } from "@mui/material";

import { TextField, MultipleSelectChip, Select } from "./../../components";

import type { Genre } from "./../../types";

type FilterContainerProps = {
  genres: Genre[];
  years: number[];
  onSearch: (queryString: string) => void;
  onSelectGenre: (genresIDs: number[]) => void;
  onSelectYear: (releaseYear: number) => void;
};

const FiltersContainer = ({
  genres,
  years,
  onSearch,
  onSelectGenre,
  onSelectYear,
}: FilterContainerProps) => {
  const onSearchHandler = (queryString: string) => {
    onSearch(queryString);
  };

  const onSelectGenreHandler = (genresIDs: number[]) => {
    onSelectGenre(genresIDs);
  };

  const onSelectYearHandler = (releaseYear: number) => {
    onSelectYear(releaseYear);
  };

  return (
    <Box width="90%" maxWidth="900px" pl={4} pr={4}>
      <Grid container>
        <Grid item xs={12}>
          <Box mb={4}>
            <TextField
              name="Alens"
              label="Search videos"
              onSearch={onSearchHandler}
            />
          </Box>
        </Grid>
        <Grid item xs={12} sm={7} mb={4}>
          <MultipleSelectChip
            genres={genres}
            label="Genre"
            onSelectGenre={onSelectGenreHandler}
          />
        </Grid>
        <Grid item xs={12} sm={5}>
          <Box mb={4}>
            <Select
              label="Year"
              years={years}
              onSelectYear={onSelectYearHandler}
            />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default FiltersContainer;
