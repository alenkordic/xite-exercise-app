import * as React from "react";

import {
  Modal as MUIModal,
  Box,
  Backdrop,
  Fade,
  Typography,
  IconButton,
  Grid,
} from "@mui/material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import InfoIcon from "@mui/icons-material/Info";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  maxWidth: 500,
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 0,
};

interface VideoDetailsProps {
  artist: string;
  title: string | number;
  release_year: number;
  image_url: string;
  genreName: string;
}

export default function Modal(videoDetails: VideoDetailsProps) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { artist, title, release_year, image_url, genreName } = videoDetails;

  return (
    <div>
      <IconButton
        sx={{ color: "rgba(255, 255, 255, 0.54)" }}
        aria-haspopup="true"
        onClick={handleOpen}
      >
        <InfoIcon />
      </IconButton>

      <MUIModal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Grid
              container
              spacing={2}
              style={{ background: "black", color: "white" }}
            >
              <Grid item xs={12}>
                <Box pr={2} pb={0} display="flex" justifyContent="flex-end">
                  <IconButton
                    aria-label="delete"
                    color="secondary"
                    onClick={handleClose}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                </Box>
                <Grid item xs={12} pr={4} pl={2}>
                  <Box>
                    <img src={image_url} alt={artist} width="100%" />
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box pl={2} mt={2}>
                    <Typography
                      component="div"
                      variant="h4"
                      fontWeight={700}
                      noWrap
                    >
                      {title}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box pl={2} style={{ color: "gray" }}>
                    <Typography
                      component="div"
                      variant="h6"
                      fontWeight={400}
                      noWrap
                    >
                      {artist}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box pr={4}>
                    <Typography
                      component="div"
                      variant="subtitle2"
                      align="right"
                    >
                      Release year: {release_year}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box pr={4} pb={4}>
                    <Typography
                      component="div"
                      variant="subtitle2"
                      align="right"
                    >
                      Genre: {genreName}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
          </Box>
        </Fade>
      </MUIModal>
    </div>
  );
}
