import {
  Card,
  Grid,
  Box,
  ImageListItem,
  ImageListItemBar,
} from "@mui/material";

import { Modal } from "./../../../components";

import useStyles from "./VideoListItem.styles";

import type { Video } from "./../../../types";

interface VideoListItemProps {
  video: Video;
  genreName: string;
}

const VideoListItem = ({ video, genreName }: VideoListItemProps) => {
  const classes = useStyles();

  const { artist, title, image_url } = video;

  return (
    <Card classes={{ root: classes.root }}>
      <Grid container>
        <Grid item xs={12}>
          <Box className={classes.imgConatainer}>
            <ImageListItem classes={{ root: classes.imagListItem }}>
              <img
                src={image_url}
                alt={title.toString()}
                loading="lazy"
                className={classes.imgConatainer}
              />
              <ImageListItemBar
                classes={{ title: classes.descriptionTitle }}
                title={title}
                subtitle={artist}
                actionIcon={<Modal genreName={genreName} {...video} />}
              />
            </ImageListItem>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};

export default VideoListItem;
