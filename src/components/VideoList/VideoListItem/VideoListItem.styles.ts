import { makeStyles, createStyles } from "@mui/styles";

export default makeStyles(() =>
  createStyles({
    root: {
      borderRadius: 3,
      overflow: "hidden",
    },
    imgConatainer: {
      overflow: "hidden",
      minHeight: "200px",
    },
    descriptionTitle: {
      fontWeight: 700,
    },
    textContainer: {
      backgroundColor: "black",
      color: "white",
    },
    imagListItem: {
      display: "flex !important",
      alignItems: "center"
    }
  })
);
