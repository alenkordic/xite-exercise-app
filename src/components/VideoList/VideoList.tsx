import { Container, Box, Grid } from "@mui/material";

import VideoListItem from "./VideoListItem";

import type { Video, Genre } from "./../../types";

interface VideolistProps {
  videos: Video[];
  genres: Genre[];
}

const VideoList = ({ videos, genres }: VideolistProps) => {
  const renderVideoList = () => {
    return videos.map((video: any) => {
      let genre = genres.filter((genre: any) => video.genre_id === genre.id)[0]
        ?.name;
      return (
        <Grid item xs={12} sm={6} md={4} lg={3} key={video.id}>
          <VideoListItem video={video} genreName={genre} />
        </Grid>
      );
    });
  };

  return (
      <Container fixed maxWidth="lg">
        <Box p={2}>
          <Grid container spacing={3}>
            {renderVideoList()}
          </Grid>
        </Box>
      </Container>
  );
};

export default VideoList;
