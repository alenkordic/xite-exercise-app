// @ts-nocheck

import React, { useState, useEffect } from "react";
import { createStyles, makeStyles } from "@mui/styles";

import {
  IconButton,
  InputAdornment,
  Chip,
  Select,
  FormControl,
  Box,
  MenuItem,
  InputLabel,
} from "@mui/material";

import CancelIcon from "@mui/icons-material/CancelOutlined";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";

import type { Genre } from "./../../types";

interface MultipleSelectChipProps {
  genres: Genre[];
  label: string;
  onSelectGenre: (genresID: number[]) => void;
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const useStyles = makeStyles((theme) =>
  createStyles({
    formControl: {
      margin: 5,
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: "flex",
      flexWrap: "wrap",
    },
    chip: {
      margin: 2,
      backgroundColor: "#d3d3d3",
    },
    noLabel: {
      marginTop: 15,
    },
  })
);

const MultipleSelectChip = ({
  genres,
  label,
  onSelectGenre,
  ...rest
}: MultipleSelectChipProps) => {
  const classes = useStyles();
  const [selectedGenres, setSelectedGenres] = useState<Genre[]>([]);

  useEffect(() => {
    const genresID = selectedGenres.map((genre: Genre) => genre.id);
    onSelectGenre(genresID);
    // eslint-disable-next-line
  }, [selectedGenres]);

  const handleChange = (event: SelectChangeEvent<Genre[]>) => {
    setSelectedGenres(event.target.value);
  };

  const handleDelete = (e: React.MouseEvent, value: Genre) => {
    e.preventDefault();
    setSelectedGenres((current: Genre[]) =>
      selectedGenres.filter((genre: Genre) => genre?.id !== value.id)
    );
  };

  return (
    <>
      <Box>
        <FormControl variant="standard" sx={{ width: "100%" }}>
          <InputLabel id="genres-chip-label" color="secondary">
            {label}
          </InputLabel>
          <Select
            endAdornment={
              selectedGenres.length > 0 && (
                <InputAdornment position="end">
                  <Box mr={2}>
                    <IconButton onClick={() => setSelectedGenres([])}>
                      <HighlightOffIcon color="secondary" />
                    </IconButton>
                  </Box>
                </InputAdornment>
              )
            }
            color="secondary"
            id="genres-chip"
            labelId="genres-chip-label"
            multiple
            value={selectedGenres}
            placeholder="Select genres"
            onChange={handleChange}
            renderValue={(itemsSelected) => (
              <div className={classes.chips}>
                {itemsSelected.map((item: Genre) => (
                  <Chip
                    key={item.id}
                    label={item.name}
                    clickable
                    color="secondary"
                    deleteIcon={
                      <CancelIcon
                        onMouseDown={(event) => event.stopPropagation()}
                      />
                    }
                    className={classes.chip}
                    onDelete={(e) => handleDelete(e, item)}
                  />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {genres.map((genre: Genre) => (
              <MenuItem key={genre.id} value={genre}>
                {genre.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
    </>
  );
};

export default MultipleSelectChip;
