import React from "react";

import {
  InputLabel,
  MenuItem,
  FormControl,
  Select as MUISelect,
  Box,
} from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";

interface SelectYearProps {
  years: number[];
  label: string;
  onSelectYear: (year: number) => void;
}
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Select = ({ years, label, onSelectYear }: SelectYearProps) => {
  const [selectedYear, setSelectedYear] = React.useState<string>("");

  const handleChange = (event: SelectChangeEvent) => {
    setSelectedYear(event.target.value);
    onSelectYear(parseInt(event.target.value));
  };

  return (
    <Box display="flex" justifyContent="flex-end">
      <FormControl variant="standard" sx={{ minWidth: 200 }}>
        <InputLabel id="year-select-label" color="secondary">
          {label}
        </InputLabel>
        <MUISelect
          color="secondary"
          labelId="year-select-label"
          id="year-select"
          value={selectedYear}
          onChange={handleChange}
          label={label}
          MenuProps={MenuProps}
        >
          <MenuItem value={0}>All years</MenuItem>
          {years.map((year) => {
            return (
              <MenuItem key={year} value={year}>
                {year}
              </MenuItem>
            );
          })}
        </MUISelect>
      </FormControl>
    </Box>
  );
};

export default Select;
