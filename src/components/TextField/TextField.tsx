import React, { useState, useEffect } from "react";
import {
  Box,
  TextField as MUITextField,
  BaseTextFieldProps,
} from "@mui/material";

interface TextFieldProps extends BaseTextFieldProps {
  name?: string;
  onSearch: (content: string) => void;
}

const TextField = ({ name, onSearch, ...rest }: TextFieldProps) => {
  const [inputValue, setInputValue] = useState<string>("");

  useEffect(() => {
    const typingPause = setTimeout(() => {
      onSearch(inputValue);
    }, 1000);
    return () => {
      clearTimeout(typingPause);
    };
    // eslint-disable-next-line
  }, [inputValue]);

  const inputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  return (
    <Box>
      <MUITextField
        name={name}
        onChange={inputChangeHandler}
        value={inputValue}
        fullWidth
        color="secondary"
        variant="standard"
        inputProps={{ min: 0, style: { textAlign: "center" } }}
        autoComplete="off"
        {...rest}
      />
    </Box>
  );
};

export default TextField;
