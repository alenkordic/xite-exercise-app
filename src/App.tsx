import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useQuery } from "react-query";

import { Loading, HomePage } from "./components";
import { getData } from "./services/getData";

import type { TData } from "./types";

const App = () => {
  const darkTheme = createTheme({
    palette: {
      mode: "dark",
    },
  });

  const { data, isLoading, isError, error } = useQuery("videos", getData, {
    retry: false,
    refetchOnWindowFocus: false,
    onSuccess: (data: TData) => {
      return data;
    },
  });

  if (isLoading || !data) {
    return <Loading />;
  }

  if (isError) {
    return <div>ERROR: {error}</div>;
  }

  return (
    <ThemeProvider theme={darkTheme}>
      <HomePage data={data} />
    </ThemeProvider>
  );
};

export default App;
