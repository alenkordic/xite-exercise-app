import axios from "axios";

export const getData = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: process.env.REACT_APP_DATASET_URL,
      responseType: "json",
    })
      .then(
        (response) => {
          return resolve(response.data);
        }
      )
      .catch(function (error) {
        if (error.response) {
          reject(error.response);
        } else if (error.request) {
          reject(error.request);
        } else {
          reject(error.message);
        }
        reject(error.config);
      });
  });
};
