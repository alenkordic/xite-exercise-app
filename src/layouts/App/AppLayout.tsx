import React from "react";
import PropTypes from "prop-types";

import useStyles from "./AppLayout.styles";

interface AppLayoutProps {
  children: React.ReactNode;
}

const AppLayout = ({ children }: AppLayoutProps) => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>{children}</div>
    </>
  );
};

AppLayout.propTypes = {
  children: PropTypes.any.isRequired
};

export default AppLayout;